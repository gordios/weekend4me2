-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2018 at 08:00 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `weekengo`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_11_190048_add_users_id_to_users_table', 2),
(4, '2018_11_15_145803_create_ui_headers_table', 3),
(5, '2018_11_21_104537_create_ui_bodies_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ui_bodies`
--

CREATE TABLE `ui_bodies` (
  `id` int(10) UNSIGNED NOT NULL,
  `headone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `headtwo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `headthree` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `headsize` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `headfour` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ui_bodies`
--

INSERT INTO `ui_bodies` (`id`, `headone`, `headtwo`, `headthree`, `headsize`, `headfour`, `path`, `created_at`, `updated_at`) VALUES
(3, 'We offer a variety of services and options', 'discover places', 'we have collected the best offers in', '4217', 'accommodation options - to see them, just enter the dates!', 'uploads/images/uibody/item-2.jpg', '2018-11-21 06:31:07', '2018-11-21 06:31:07'),
(4, 'Visit museum with a dedicated tour guide', 'city tours', 'we have collected the best offers in', '4217', 'accommodation options - to see them, just enter the dates!', 'uploads/images/uibody/item-3.jpg', '2018-11-21 06:35:42', '2018-11-21 06:35:42'),
(5, 'City tours / tour tickets / tour guides', 'more tours', 'city tours', '4217', 'accommodation options - to see them, just enter the dates!', 'uploads/images/uibody/item-4.jpg', '2018-11-21 06:37:34', '2018-11-21 06:37:34'),
(7, 'Looking for something amazing?', 'your best trip', 'we have collected the best offers in', '4217', 'accommodation options - to see them, just enter the dates!', 'uploads/images/uibody/item-3.jpg', '2018-11-21 13:51:00', '2018-11-21 13:51:00');

-- --------------------------------------------------------

--
-- Table structure for table `ui_headers`
--

CREATE TABLE `ui_headers` (
  `id` int(10) UNSIGNED NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logoheading` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `supportnum` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ui_headers`
--

INSERT INTO `ui_headers` (`id`, `path`, `logoheading`, `supportnum`, `created_at`, `updated_at`) VALUES
(1, 'uploads/images/uihead/air-br.jpg', 'Testing', 'testing@gmail.com', '2018-11-15 23:01:54', '2018-11-21 13:49:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'test', 'test@gmail.com', '$2y$10$ptv35lok243y1xJRL7uKiejfQgSR3G95bVkT/E95i2U0sXmQWjF4G', 'BJw5saXDSp5bytCkULzpXYrULZT4mPhyIOA9HjlTuMHhu0kfykotc7EgbX8t', '2018-11-01 08:29:38', '2018-11-01 08:29:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `ui_bodies`
--
ALTER TABLE `ui_bodies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ui_headers`
--
ALTER TABLE `ui_headers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ui_bodies`
--
ALTER TABLE `ui_bodies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ui_headers`
--
ALTER TABLE `ui_headers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
