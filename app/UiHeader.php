<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UiHeader extends Model
{
    protected $guarded = ['id'];
}
