<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use App\UiHeader;
use App\UiBody;
use session;
//use Illuminate\Support\Facades\Session ;

class FrontController extends Controller
{
    public function index()
    {
      App::setLocale(Session::get('locale'));
      $data['uiheaders'] = UiHeader::all();
      $data['uibodies'] = UiBody::all();
      return view('front.index',$data);
    }

    public function deals()
    {
      return view('front.deals');
    }

    public function place()
    {
      return view('front.place');
    }

    public function flights()
    {
      return view('front.flights');
    }

    public function news()
    {
      return view('front.news');
    }

    public function about()
    {
      return view('front.about');
    }

    public function contact()
    {
      return view('front.contact');
    }

    public function change_language($locale = null)
    {
        Session::put('locale', $locale);
        return Session::get('locale');
        //return Redirect::back();
        //App::setLocale($locale);
        //echo $locale;exit;
    }



}
