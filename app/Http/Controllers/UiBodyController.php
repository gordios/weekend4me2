<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UiBody;

class UiBodyController extends Controller
{
    public function index()
    {
        $data['uibodies'] = UiBody::all();
        return view('admin.ui.body',$data);
    }

    public function view()
    {

        return view('admin.ui.bodyform');
    }

    public function store(Request $request)
    {
      $uibody = UiBody::firstorNew(['id'=>$request->id]);
      $uibody->headone = $request->headone;
      $uibody->headtwo = $request->headtwo;
      $uibody->headthree = $request->headthree;
      $uibody->headsize = $request->headsize;
      $uibody->headfour = $request->headfour;
      if ($request->hasFile('path'))
      {
          $file=$request->file('path');
          $path = 'uploads/images/uibody/'; //We are not defined "/" here because it is represent root directory.
          $filename = $file->getClientOriginalName();
          $file->move($path, $filename);
             $uibody->path = ($path.$filename);
        }

         $uibody->save();
         return redirect('/ui/body');
    }

    public function delete($id)
    {
      $uibody = UiBody::find($id)->delete();
      return back();
    }

}
