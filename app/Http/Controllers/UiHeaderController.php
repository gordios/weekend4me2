<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UiHeader;

class UiHeaderController extends Controller
{
    public function index()
    {
      $data['headers'] = UiHeader::where('id',1)->first();
      return view('admin.ui.head',$data);
    }

    public function store(Request $request)
    {
     $uiheader = UiHeader::firstorNew(['id'=>$request->id]);
     if ($request->hasFile('path'))
     {
         $file=$request->file('path');
         $path = 'uploads/images/uihead/'; //We are not defined "/" here because it is represent root directory.
         $filename = $file->getClientOriginalName();
         $file->move($path, $filename);
            $uiheader->path = ($path.$filename);
       }
        $uiheader->logoheading = $request->logoheading;
        $uiheader->supportnum = $request->supportnum;
        $uiheader->save();
        return redirect('/ui/head');
    }

}
