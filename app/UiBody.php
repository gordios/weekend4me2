<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UiBody extends Model
{
  protected $guarded = ['id'];
}
