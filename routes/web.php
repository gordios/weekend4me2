<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Route::get('setlocale/{locale}', function ($locale) {
  if (in_array($locale, \Config::get('app.locales'))) {
    Session::put('locale', $locale);
  }
  return redirect()->back();
});*/

Route::get('/', 'FrontController@index');
Route::get('/deals', 'FrontController@deals');
Route::get('/place', 'FrontController@place');
Route::get('/flights', 'FrontController@flights');
Route::get('/news', 'FrontController@news');
Route::get('/about', 'FrontController@about');
Route::get('/contact', 'FrontController@contact');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/ui/body', 'UiBodyController@index');
Route::get('/ui/body/form', 'UiBodyController@view');
Route::post('/ui/body/submit', 'UiBodyController@store');
Route::get('/ui/body/delete/{id}', 'UiBodyController@delete');

Route::get('/ui/head', 'UiHeaderController@index');
Route::post('/ui/head/submit', 'UiHeaderController@store');


Route::get('/ui/footer', function () {
    return view('admin.ui.footer');
});

Route::get('/admin/profile', function () {
    return view('admin.profile');
});

Route::get('/change_locale/{locale}', 'FrontController@change_language');
