<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Looking for something amazing?">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="True">
    <title>Weekengo Demo</title>
    <!-- Edge and IE-->
    <meta name="msapplication-tap-highlight" content="no">
    <!-- Add to homescreen for Chrome on Android-->
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="application-name" content="Looking for something amazing?">
    <link rel="icon" sizes="192x192" href="/flight/img/touch/chrome-touch-icon-192x192.png">
    <!-- Add to homescreen for Safari on iOS-->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Your Travel World">
    <link rel="apple-touch-icon" href="/flight/img/touch/apple-touch-icon.png">
    <!-- Google Fonts-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,500,600,700%7CPoppins:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="/flight/css/jquery-ui.structure.min.css">
    <link rel="stylesheet" href="/flight/css/jquery-ui.theme.min.css">
    <link rel="stylesheet" href="/flight/css/bootstrap.min.css">
    <link rel="stylesheet" href="/flight/css/custom.css">
    <link id="cssRtl" rel="stylesheet" href="#">
    <link rel="stylesheet" href="/flight/css/flatpickr.min.css">
    <link rel="stylesheet" href="/flight/css/font-awesome.min.css">
    <link rel="stylesheet" href="/flight/css/fontawesome-stars-o.css">
    <link rel="stylesheet" href="/flight/css/swiper.min.css">
    <link rel="stylesheet" href="/flight/css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" href="/flight/css/select2.min.css">
    <link rel="stylesheet" href="/flight/css/nouislider.min.css">
    <link rel="stylesheet" href="/flight/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="/flight/css/nice-select.css">
    <link rel="stylesheet" href="/flight/style.min.css">
    <link id="cssTheme" rel="stylesheet" href="/flight/skins/style-default.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
  </head>
  <body class="load">
    <div class="progress-load js-progress-load"></div>
    <div class="modal-account modal" id="modalAccount" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <ul class="modal-account__tabs nav nav-tabs d-flex js-account-tabs" id="accountTabs" role="tablist">
              <li class="nav-item w-50"><a class="nav-link" data-toggle="tab" href="#accountLogin" role="tab" aria-controls="accountLogin" aria-selected="false">Login</a></li>
              <li class="nav-item w-50"><a class="nav-link" data-toggle="tab" href="#accountRegist" role="tab" aria-controls="accountRegist" aria-selected="false">Sign Up</a></li>
              <li><a class="nav-link p-0 border-0" data-toggle="tab" href="#accountForgot" role="tab" aria-controls="accountForgot" aria-selected="false"></a></li>
            </ul>
            <button class="btn btn-light close pointer" type="button" data-dismiss="modal" aria-label="close"><span class="icon text-dark" aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510"><path d="M255 0C114.75 0 0 114.75 0 255s114.75 255 255 255 255-114.75 255-255S395.25 0 255 0zm127.5 346.8l-35.7 35.7-91.8-91.8-91.8 91.8-35.7-35.7 91.8-91.8-91.8-91.8 35.7-35.7 91.8 91.8 91.8-91.8 35.7 35.7-91.8 91.8 91.8 91.8z"/></svg></span></button>
          </div>
          <div class="modal-body">
            <div class="tab-content">
              <div class="tab-pane" id="accountLogin" role="tabpanel">
                <form class="modal-account__form js-account-form" id="formLogin"  action="{{ route('login') }}" method="POST" data-toggle="validator">
                      {{ csrf_field() }}
                  <div class="form-group">
                    <input class="form-control form-control-sm" type="email" name="user_email" placeholder="E-mail address" required="required"/>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <input class="form-control form-control-sm" type="password" name="user_pass" placeholder="Password" data-minlength="6" required="required"/>
                    <div class="help-block">Your password must be at least 6 characters long</div>
                  </div>
                  <div class="d-flex flex-wrap justify-content-between align-items-center">
                    <button class="btn btn-secondary btn--round mr-2 mb-2" type="submit">sign in
                    </button>
                    <p class="mb-2"><a class="js-toggle-account" href="#" data-account="forgot">Forgot your password?</a></p>
                  </div>
                  <div class="d-inline-block my-2 w-100">
                    <div class="social-sign">
                      <div class="divider divider-horizontal">
                        <h5 class="font-weight-normal mx-2">Or click to sign in</h5>
                      </div>
                      <ul class="social-sign__list row">
                        <li class="col col-sm-6"><a class="nav-link" href="#"><img class="nav-icon img-fluid mr-2" src="/flight/img/facebook.png" alt="#"/><span class="nav-text">Connect with Facebook</span></a></li>
                        <li class="col col-sm-6"><a class="nav-link" href="#"><img class="nav-icon img-fluid mr-2" src="/flight/img/google-plus.png" alt="#"/><span class="nav-text">Connect with Google</span></a></li>
                      </ul>
                    </div>
                  </div>
                </form>
                <div class="modal-footer d-block">
                  <p class="fz-small mb-0"><em>By accessing your account, you agree to our  <a href="#">Terms and Conditions</a> and   <a href="#">Privacy Policy</a></em></p>
                </div>
              </div>
              <div class="tab-pane active show" id="accountRegist" role="tabpanel">
                <form class="modal-account__form js-account-form" id="formRegist" action="{{ route('register') }}" method="POST" data-toggle="validator">
                      {{ csrf_field() }}
                  <div class="form-group">
                    <input class="form-control form-control-sm" type="text" placeholder="First Name" name="name" value="{{ old('name') }}" required autofocus>
                    <div class="help-block">Please enter your name</div>
                  </div>
                  <div class="form-group">
                    <input class="form-control form-control-sm" type="email" name="email" value="{{ old('email') }}" placeholder="E-mail address" required="required"/>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <input class="form-control form-control-sm" id="userPass" type="password" name="password" placeholder="Password" data-minlength="6" required="required"/>
                    <div class="help-block">Your password must be at least 6 characters long</div>
                  </div>
                  <div class="form-group">
                    <input class="form-control form-control-sm" type="password" name="password_confirmation" placeholder="Confirm Password" data-match="#userPass" required="required"/>
                    <div class="help-block">Please enter the same password as above</div>
                  </div>
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input class="custom-control-input" id="agreePolicy" type="checkbox" required><span class="custom-control-label">I have read and agree to the  <a href="#">Terms of Use</a> and the  <a href="#">Privacy Policy.</a></span>
                    </label>
                    <div class="help-block">Please accept our policy</div>
                  </div>
                  <p class="mb-4 d-flex justify-content-center justify-content-sm-start">
                    <button class="btn btn-secondary btn--round" type="submit">Create Account</button>
                  </p>
                  <div class="d-inline-block my-2 w-100">
                    <div class="social-sign">
                      <div class="divider divider-horizontal">
                        <h5 class="font-weight-normal mx-2">Or click to sign in</h5>
                      </div>
                      <ul class="social-sign__list row">
                        <li clasa s="col col-sm-6"><a class="nav-link" href="#"><img class="nav-icon img-fluid mr-2" src="/flight/img/facebook.png" alt="#"/><span class="nav-text">Sign up with Facebook</span></a></li>
                        <li class="col col-sm-6"><a class="nav-link" href="#"><img class="nav-icon img-fluid mr-2" src="/flight/img/google-plus.png" alt="#"/><span class="nav-text">Sign up with Google</span></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="modal-footer d-block">
                    <div class="form-group">
                      <label class="custom-control custom-checkbox">
                        <input class="custom-control-input" id="dispatchEmail" type="checkbox"><span class="custom-control-label">Please send me Travelocity.com emails with travel deals, special offers, and other information.</span>
                      </label>
                    </div>
                  </div>
                </form>
              </div>
              <div class="tab-pane" id="accountForgot" role="tabpanel">
                <form class="modal-account__form" id="formForgot" action="#" method="POST" data-toggle="validator">
                  <div class="form-group">
                    <input class="form-control form-control-sm" type="email" name="user_email" placeholder="E-mail address" required>
                    <div class="help-block with-errors"></div>
                  </div>
                  <p class="text-center">We will email you instructions for resetting your password.</p>
                  <p class="d-flex justify-content-center">
                    <button class="btn btn-secondary btn--round" type="submit">Retrieve Password
                    </button>
                  </p>
                  <p class="d-flex justify-content-center"><a class="js-toggle-account mr-3" href="#" data-account="regist">Create a new account</a><a class="js-toggle-account" href="#" data-account="login">Return to Sign In</a></p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <header class="page-header">
    @include('front.navbar')
    </header>
    <main class="page-main">
    @yield('content')
    </main>
    <footer class="page-footer">
      @include('front.footer')
    </footer>
    <div class="modal-map modal" id="modalMap" tabindex="-1">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header align-items-center py-2">
            <h4 class="modal-title"><a class="d-flex align-items-center" href="hotel.html"><i class="icon mr-2"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.5 22.1"><path d="M0.3,10.3C0.3,10.3,0.3,10.3,0.3,10.3c-0.4,0.5-0.4,1.2,0,1.6l10,10c0,0,0,0,0,0c0.4,0.4,1.1,0.4,1.6,0 c0.4-0.4,0.4-1.1,0-1.6l-8-8h26.6c0.6,0,1.1-0.5,1.1-1.1c0,0,0,0,0,0c0-0.6-0.5-1.1-1.1-1.1H3.8l8-8c0.4-0.4,0.4-1.1,0-1.6 c0,0,0,0,0,0c-0.4-0.4-1.1-0.4-1.6,0L0.3,10.3z"/></svg></i><span class="title">Hotel</span></a></h4>
            <button class="btn btn-secondary btn--round px-4" type="button" data-dismiss="modal">map close
            </button>
          </div>
          <div class="map-contain" id="map"></div>
        </div>
      </div>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3gFKhVywUkygSxQEBdGVrI5-ZRrdjueA"></script>
    <script src="/flight/js/maps.js"></script>
    <script src="/flight/js/libs/moment.min.js"></script>
    <script src="/flight/js/libs/wNumb.js"></script>
    <script src="/flight/js/libs/nouislider.min.js"></script>
    <script src="/flight/js/libs/jquery.min.js"></script>
    <script src="/flight/js/libs/jquery-ui.min.js"></script>
    <script src="/flight/js/libs/barba.min.js"></script>
    <script src="/flight/js/libs/sticky-kit.min.js"></script>
    <script src="/flight/js/libs/velocity.min.js"></script>
    <script src="/flight/js/libs/velocity.ui.min.js"></script>
    <script src="/flight/js/libs/jquery.waypoints.min.js"></script>
    <script src="/flight/js/libs/popper.min.js"></script>
    <script src="/flight/js/libs/bootstrap.min.js"></script>
    <script src="/flight/js/libs/imagesloaded.pkgd.min.js"></script>
    <script src="/flight/js/libs/masonry.pkgd.min.js"></script>
    <script src="/flight/js/libs/isotope.pkgd.min.js"></script>
    <script src="/flight/js/libs/ofi.min.js"></script>
    <script src="/flight/js/libs/jarallax.min.js"></script>
    <script src="/flight/js/libs/jarallax-video.min.js"></script>
    <script src="/flight/js/libs/jarallax-element.min.js"></script>
    <script src="/flight/js/libs/jquery.mCustomScrollbar.min.js"></script>
    <script src="/flight/js/libs/swiper.min.js"></script>
    <script src="/flight/js/libs/flatpickr/flatpickr.min.js"></script>
    <script src="/flight/js/libs/flatpickr/rangePlugin.js"></script>
    <script src="/flight/js/libs/select2.min.js"></script>
    <script src="/flight/js/libs/select2/en.js"></script>
    <script src="/flight/js/libs/jquery.mask.min.js"></script>
    <script src="/flight/js/libs/validator.min.js"></script>
    <script src="/flight/js/libs/jquery.barrating.min.js"></script>
    <script src="/flight/js/libs/jquery.blueimp-gallery.min.js"></script>
    <script src="/flight/js/libs/jquery.nice-select.min.js"></script>
    <script src="/flight/js/script.min.js"></script>
    <script src="/flight/js/demo-switcher.js"></script>
    <script src="/js/custom.js"></script>
  </body>
</html>
