@extends('layouts.admin')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Weekend4me <small> Changes layout for header part by Admin</small></h5>
                        </div>
                        <div class="ibox-content">
                            <form method="post" action="/ui/head/submit" enctype="multipart/form-data" class="form-horizontal">
                              {{ csrf_field() }}
                                <div class="form-group"><label class="col-sm-2 control-label">Upload Logo:</label>
                                    <div class="col-sm-10"><input type="file" name="path" class="form-control">
                                        </div>
                                </div>
                                @if(isset($headers) &&  !empty($headers->path))
                                  <img src="/{{$headers->path}}">
                                @endif
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Logo Heading:</label>
                                    <div class="col-sm-10"><input type="text" name="logoheading" class="form-control"></div>
                                </div>
                                @if(isset($headers) &&  !empty($headers->logoheading))
                                  <h2>{{$headers->logoheading}}</h2>
                                @endif
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Contact Support Number:</label>
                                    <div class="col-sm-10"><input type="text" name="supportnum" class="form-control"></div>
                                </div>
                                @if(isset($headers) &&  !empty($headers->supportnum))
                                  <h2>{{$headers->supportnum}}</h2>
                                @endif
                                <div class="hr-line-dashed"></div>
                                <input type="hidden" name="id" value="1">
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">Cancel</button>
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
