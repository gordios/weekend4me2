@extends('layouts.admin')
@section('content')
<div class="row">
                <div class="col-lg-12">
                  <div class="ibox">
                        <div class="ibox-title">
                            <h5>All Images and text for Body Part</h5>
                            <div class="ibox-tools">
                              <a href="/ui/body/form" class="btn btn-primary btn-xs">Create new post</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="project-list">
                                <table class="table table-hover">
                                    <tbody>
                                      @foreach($uibodies as $uibody)
                                    <tr>
                                        <td class="project-status">
                                            <span class="label label-primary">{{$uibody->id}}</span>
                                        </td>
                                        <td class="project-title">
                                            <a href="#">{{$uibody->headone}}</a>
                                            <br>
                                            <small>{{$uibody->headtwo}}</small>
                                        </td>
                                        <td class="project-completion">
                                                <small>  {{$uibody->headthree}}</small>
                                                <small>{{$uibody->headsize}}</small>
                                              <small>{{$uibody->headfour}}</small>
                                        </td>
                                        <td class="project-people">
                                            <a href=""><img alt="image" class="img-circle" src="/{{$uibody->path}}"></a>
                                        </td>
                                        <td class="project-actions">
                                            <a href="/ui/body/delete/{{$uibody->id}}" name="id" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i>Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection
