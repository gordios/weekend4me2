@extends('layouts.admin')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Weekend4me <small> Changes layout for Admin</small></h5>
                        </div>
                        <div class="ibox-content">
                            <form method="post" action="/admin/profile/submit" enctype="multipart/form-data" class="form-horizontal">
                              {{ csrf_field() }}
                              <div class="form-group"><label class="col-sm-2 control-label">Upload Image:</label>
                                  <div class="col-sm-10"><input type="file" name="path" class="form-control"></div>
                              </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Uploading Heading2:</label>
                                    <div class="col-sm-10"><input type="text" name="headtwo" class="form-control"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Uploading Heading3:</label>
                                    <div class="col-sm-10"><input type="text" name="headthree" class="form-control"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Uploading Size:</label>
                                    <div class="col-sm-10"><input type="text" name="headsize" class="form-control"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group"><label class="col-sm-2 control-label">Uploading Heading4:</label>
                                    <div class="col-sm-10"><input type="text" name="headfour" class="form-control"></div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-white" type="submit">Cancel</button>
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
@endsection
