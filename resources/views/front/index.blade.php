@extends('layouts.master')
@section('content')
<section class="intro d-flex flex-column load">
  <div class="intro__bg js-intro-bg">
    <div class="over"></div>
    <div class="swiper-container js-intro-slider-bg">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
          @foreach($uibodies as $uibody)
          <img class="img-cover" src="/{{$uibody->path}}" alt="#">
          @endforeach
        </div>
      </div>
    </div>
  </div>
  <div class="intro__desc js-intro-slider-desc swiper-container">
    @foreach($uibodies as $uibody)
    <div class="swiper-wrapper align-items-center">
      <div class="swiper-slide">
        <div class="container w-100 text-center">
          <div class="row">
            <div class="col-12 col-lg-10 mx-auto">
              <h4 class="h4 intro__caption prlx-scroll">{{$uibody->headone}}</h4>
              <h1 class="h1 intro__title prlx-scroll">{{$uibody->headtwo}}</h1>
              <p class="prlx-scroll">
                {{$uibody->headthree}}
                <span class="js-counter">{{$uibody->headsize}}</span>
                {{$uibody->headfour}}
              </p>
            </div>
          </div>
        </div>
      </div>

    </div>
    @endforeach
    <button class="intro__btn-scroll js-intro-btn-jump" type="button"><span></span></button>
  </div>
  <div class="intro__content d-flex flex-column justify-content-end js-intro-content">
    <div class="intro__search">
      <div class="container">
        <div class="search-hotels shadow-sm">
          <!-- <ul class="search-hotels__tabs nav nav-tabs" role="tablist">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tabHotel" role="tab" aria-controls="tabHotel" aria-selected="false"><span class="d-none d-md-block">Hotel</span><i class="icon icon-building d-md-none mx-1 text-secondary"></i></a>
            </li>
          </ul> -->
          <div class="tab-content">
            <div class="tab-pane active show" id="tabHotel" role="tabpanel">
              <form class="search-hotels__form" action="#" method="GET" data-toggle="validator">
                <div class="row">
                  <div class="form-group col-12 col-md-6 col-lg-4 col-xl-3">
                    <label class="label-text">Departure Airport</label>
                    <div class="d-flex" data-toggle="modal" data-target="#myModal" style="cursor:pointer;"><i class="fa fa-plane mr-2 text-secondary"></i>
                      From any Airport
                        <!-- <select class="select2 js-select-locality" name="search_hotel" data-placeholder="Enter here a place or hotel">
                          <option value="Resort Spa Hotel">Resort Spa Hotel</option>
                          <option value="Black Pearl">Black Pearl</option>
                          <option value="Marsol">Marsol</option>
                          <option value="Macronissos Village Bungalows">Macronissos Village Bungalows</option>
                          <option value="Iberotel Aquamarine Resort">Iberotel Aquamarine Resort</option>
                        </select> -->
                    </div>
                <!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="margin-top: 100px;width:600px;height:540px;">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Select Your Departure Airport</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="overflow:scroll;">
          <div class="serach">
          <form action="/action_page.php">
           <input type="text" placeholder="Search for departure airport" name="search" style="width: 100%;background-color:#F1F1F1;">
           <button type="submit"></button>
          </form>
        </div>
        <div class="country">
        <ul>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
            <li class="modalli" style="height: 32px;background:color:#f1f1f1;"><span class="imgli"><img src="flight/img/au.png" style="height: 20px;margin-left: 6px;margin-top: 6px;"></span>
            <span class="livalue" style="margin-left:10px;">AmesterDam</span>
            <strong style="float:right;">(AMS)</strong>
            </li>
        </ul>
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="days" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="margin-top: 100px;width:620px;height:350px;">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Select Your Travels Days</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="serach">
           <i class="mr-2 icon icon-calendar text-secondary"></i><h4 style="margin-left: 24px;margin-top: -22px;">Travels days for your weekend</h4>
        </div>
        <div class="country">
        <button type="button" class="btn btn-success" style="width:20%;padding-left: 10px;margin-top:10px;cursor:pointer;">Sunday</button>
        <button type="button" class="btn btn-success" style="width:20%;padding-left: 10px;margin-top:10px;cursor:pointer;">Monday</button>
        <button type="button" class="btn btn-success" style="width:20%;padding-left: 10px;margin-top:10px;cursor:pointer;">Tuesday</button>
        <button type="button" class="btn btn-success" style="width:20%;padding-left: 10px;margin-top:10px;cursor:pointer;">WednesDay</button>
        <button type="button" class="btn btn-success" style="width:20%;padding-left: 10px;margin-top:10px;cursor:pointer;">Thursday</button>
        <button type="button" class="btn btn-success" style="width:20%;padding-left: 10px;margin-top:10px;cursor:pointer;">Friday</button>
        <button type="button" class="btn btn-success" style="width:20%;padding-left: 10px;margin-top:10px;cursor:pointer;">Saturday</button>
        </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="travellers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content" style="margin-top:100px;width:550px;height:320px;">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Select Numbers of Travellers</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group col-12 col-lg-4 col-xl-3 d-flex justify-content-center" style="margin-left: 180px;">
            <div class="mx-2" style="padding-left:25px;">
              <label class="label-text">Adults</label>
              <div class="qty">
                <input class="js-qty" type="text" name="rooms" value="2" data-mask="00"/>
              </div>
            </div>
            <div class="mx-2" style="padding-left:25px;">
              <label class="label-text">Children</label>
              <div class="qty">
                <input class="js-qty" type="text" name="adults" value="2" data-mask="00"/>
              </div>
            </div>
            <div class="mx-2" style="padding-left:25px;">
              <label class="label-text">Infants</label>
              <div class="qty">
                <input class="js-qty" type="text" name="children" value="2" data-mask="00"/>
              </div>
            </div>
            <button class="btn btn-secondary btn--round align-self-center" type="submit" style="margin-top: 22px;margin-left: 50px;">Search
            </button>
          </div>
          <h2 style="margin-top: 20px;margin-left: 50px;font-size: 20px;"> 2 Adults in a Room ?</h2>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>

                  </div>
                  <div class="form-group col-12 col-md-6 col-lg-4 col-xl-4 d-flex justify-content-center">
                    <div class="form-group-date text-nowrap text-center">
                      <div class="d-inline-block" data-toggle="modal" data-target="#days" style="cursor:pointer;">
                        <label class="label-text">Travels Days</label>
                        <div class="input-date-group position-relative"><i class="mr-2 icon icon-calendar text-secondary"></i>
                          Sunday-Saturday
                          <!-- <input class="form-control js-input-date hidden " id="hotelDate1" type="text" name="hotel_from" required="required"/> -->
                          <label class="form-control date" for="hotelDate1"></label>
                        </div>
                        <!-- Modal -->
                      <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog">
                          <!-- Modal content-->
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Modal Header</h4>
                            </div>
                            <div class="modal-body">
                              <p>Some text in the modal.</p>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group col-12 col-lg-4 col-xl-3 d-flex justify-content-center">
                    <!-- <div class="mx-2">
                      <label class="label-text">Rooms</label>
                      <div class="qty">
                        <input class="js-qty" type="text" name="rooms" value="2" data-mask="00"/>
                      </div>
                    </div> -->
                    <div class="mx-2">
                      <label class="label-text">Travellers</label>
                      <div class="qty" data-toggle="modal" data-target="#travellers" style="cursor:pointer;">
                        <i class="mr-2 icon icon-calendar text-secondary"></i>
                        Two-Adults
                      </div>
                      <!-- Modal -->
                    <div class="modal fade" id="myModal" role="dialog">
                      <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modal Header</h4>
                          </div>
                          <div class="modal-body">
                            <p>Some text in the modal.</p>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                    <!-- <div class="mx-2">
                      <label class="label-text">Children</label>
                      <div class="qty">
                        <input class="js-qty" type="text" name="children" value="2" data-mask="00"/>
                      </div>
                    </div> -->
                  </div>
                  <div class="form-group col-12 col-xl-2 d-flex">
                    <button class="btn btn-secondary btn--round align-self-center" type="submit">Search
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="intro__hotels">
      <div class="container-fluid p-0">
        <div class="swiper-container js-intro-hotels">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="card-intro d-block" style="background-image: url(flight/img/hotels/item-1.jpg);">
                <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                <div class="card-intro__footer">
                  <h4 class="h4 f-primary">Budapest</h4>
                  <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Thailand</span></div>
                  <div class="card-intro__rating">
                    <select class="js-rating-stat" data-current-rating="5">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5" selected="selected">5</option>
                    </select>
                  </div>
                </div>
                <div class="card-hover">
                  <h3 class="h3 text-uppercase">Budapest</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-intro d-block" style="background-image: url(flight/img/hotels/item-3.jpg);">
                <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                <div class="card-intro__footer">
                  <h4 class="h4 f-primary">Berlin</h4>
                  <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Berlin</span></div>
                  <div class="card-intro__rating">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
                </div>
                <div class="card-hover">
                  <h3 class="h3 text-uppercase">Majorca</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-intro d-block" style="background-image: url(flight/img/hotels/item-3.jpg);">
                <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                <div class="card-intro__footer">
                  <h4 class="h4 f-primary">Majorca</h4>
                  <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Costa Brava, Spain</span></div>
                  <div class="card-intro__rating">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
                </div>
                <div class="card-hover">
                  <h3 class="h3 text-uppercase">Marsol</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-intro d-block" style="background-image: url(flight/img/hotels/item-4.jpg);">
                <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                <div class="card-intro__footer">
                  <h4 class="h4 f-primary">Macronissos Village Bungalows</h4>
                  <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Ayia Napa, Cyprus</span></div>
                  <div class="card-intro__rating">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
                </div>
                <div class="card-hover">
                  <h3 class="h3 text-uppercase">Macronissos Village Bungalows</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                </div>
              </div>
            </div>
            <div class="swiper-slide">
              <div class="card-intro d-block" style="background-image: url(flight/img/hotels/item-1.jpg);">
                <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
                <div class="card-intro__footer">
                  <h4 class="h4 f-primary">Berlin</h4>
                  <div class="card-intro__local d-flex align-items-center"><i class="icon icon-label mr-1"></i><span>Hurghada, Egypt</span></div>
                  <div class="card-intro__rating">
                    <select class="js-rating-stat" data-current-rating="4">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4" selected="selected">4</option>
                      <option value="5">5</option>
                    </select>
                  </div>
                </div>
                <div class="card-hover">
                  <h3 class="h3 text-uppercase">Geneva</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="intro__hotels-controls">
          <button class="btn btn-primary btn-nav btn-nav--left js-prev" type="button"><i class="fa fa-angle-left"></i></button>
          <button class="btn btn-primary btn-nav btn-nav--right js-next" type="button"><i class="fa fa-angle-right"></i></button>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="page-content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 text-center">
        <div class="section-header">
          <h2 class="h2">{{ trans('language.title') }}</h2>
          <div class="section-header__stars mb-3"><i class="fa fa-star"></i><i class="fa fa-star center"></i><i class="fa fa-star"></i></div>
          <p class="fz-norm mb-0"><em>{{ trans('language.subtitle') }}</em></p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-7.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Saint Peterburg</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Hotels</li>
                <li class="amout">
                </li>
                <select class="js-rating-stat" data-current-rating="4">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4" selected="selected">4</option>
                  <option value="5">5</option>
                </select>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Saint Peterburg</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-8.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Milan</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Italy</li>
                <select class="js-rating-stat" data-current-rating="4">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4" selected="selected">4</option>
                  <option value="5">5</option>
                </select>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Milan</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-9.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Barselona</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Spain</li>
                <li class="amout">
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Barselona</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-10.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Rio de Janeiro</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Brazil</li>
                <li class="amout">
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
            <ul class="d-inline-flex flex-wrap">
              <li class="mr-4">Departure</li>
              <li class="amout">
                Fri 18/01/19 at 7:25
              </li>
            </ul>
          </div>
          <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
            <ul class="d-inline-flex flex-wrap">
              <li class="mr-4">Return</li>
              <li class="amout">
                Sun 21/01/19 at 7:25
              </li>
            </ul>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Rio de Janeiro</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-11.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Paris</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">England</li>
                <li class="amout">
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Paris</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-12.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Barselona</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">France</li>
                <li class="amout">
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Barselona</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-13.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Amsterdam</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Netherlands</li>
                <li class="amout">
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Amsterdam</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-14.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Berlin</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Germany</li>
                <li class="amout">
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Berlin</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-hotel w-100 mb-2">
          <div class="card-hotel__img"><img class="img-cover" src="flight/img/hotels/item-15.jpg" alt="#"/>
          </div>
          <div class="card-price"><span class="mr-1">from</span><span class="count text-secondary">300$</span></div>
          <div class="card-hotel__bottom">
            <h4 class="h4 mb-1">Budapest</h4>
            <div class="card-hotel__local d-flex align-items-center"><i class="icon icon-label text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Hungary</li>
                <li class="amout">
                  <select class="js-rating-stat" data-current-rating="4">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4" selected="selected">4</option>
                    <option value="5">5</option>
                  </select>
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Departure</li>
                <li class="amout">
                  Fri 18/01/19 at 7:25
                </li>
              </ul>
            </div>
            <div class="card-hotel__local d-flex align-items-center"><i class="fa fa-plane text-secondary mr-1"></i>
              <ul class="d-inline-flex flex-wrap">
                <li class="mr-4">Return</li>
                <li class="amout">
                  Sun 21/01/19 at 7:25
                </li>
              </ul>
            </div>
          </div>
          <div class="card-hover">
            <h3 class="h3 text-uppercase">Budapest</h3><a class="btn btn-light btn--round card-hover__view" href="/place">VIEW</a>
          </div>
        </div>
      </div>
      <div class="col-12 page-section__more text-center">
        <button class="btn btn-secondary btn--round btn-load" type="button">Show more<i class="fa fa-spin"></i>
        </button>
      </div>
    </div>
    <div class="row">
      <div class="col-12 text-center">
        <div class="section-header">
          <h2 class="h2">The opportunities we provide</h2>
          <div class="section-header__stars mb-3"><i class="fa fa-star"></i><i class="fa fa-star center"></i><i class="fa fa-star"></i></div>
          <p class="fz-norm mb-0"><em>Explore the features and benefits of our service</em></p>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-service w-100 mb-2">
          <div class="card-header">
            <h4 class="h4">Handpicked Hotels</h4>
          </div>
          <div class="card-body">
            <p>All hotels on our website are tested according to various criteria. You can be sure of your choice.</p>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-service w-100 mb-2">
          <div class="card-header">
            <h4 class="h4">Detailed Descriptions</h4>
          </div>
          <div class="card-body">
            <p>In order for you to have the most complete idea about the hotel, we try to collect the most complete and detailed description.</p>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-service w-100 mb-2">
          <div class="card-header">
            <h4 class="h4">Best Price Guarantee</h4>
          </div>
          <div class="card-body">
            <p>We offer the best hotels at the best prices. If you find the same room category on the same dates cheaper elsewhere, we will refund the difference. </p>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-service w-100 mb-2">
          <div class="card-header">
            <h4 class="h4">Secure Booking</h4>
          </div>
          <div class="card-body">
            <p>Book hotels with us easily and safely. All data on your credit card is encrypted and secure. You can be calm for your means.</p>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-service w-100 mb-2">
          <div class="card-header">
            <h4 class="h4">Better service</h4>
          </div>
          <div class="card-body">
            <p>Our specialists visit various hotels to personally assess their quality and provide you with a detailed review.</p>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-lg-4 d-flex mb-4">
        <div class="card card-service w-100 mb-2">
          <div class="card-header">
            <h4 class="h4">Any Questions?{{__('language.welcome')}}</h4>
          </div>
          <div class="card-body">
            <p>Call us at 8-800-2000-6000 and we will answer your questions, help you find a hotel and make a reservation. Working 24/7</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <button class="btn btn-primary btn-nav btn-nav--up js-scroll-up" type="button"><i class="fa fa-angle-up"></i></button>
</div>
@endsection
